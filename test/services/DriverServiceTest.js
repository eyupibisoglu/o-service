var globals = require('../../globals.js')
var SERVICE_NAME    = 'drivers'
var SERVICE_PATH    = '/api/v1/' + SERVICE_NAME 

/* ### */

var Mongoose   = require('mongoose')
var connection = Mongoose.createConnection( globals.database() );
var Driver     = connection.model("Driver", require('../../models/Driver'));
/* ### */

let chai   = require('chai');
let server = require('../../app');
let should = chai.should();
let Async  = require('async');

chai.use(require('chai-http'));


before(function(done)
{
	Async.parallel(
	[
		( callback ) => Driver.create({ "_id": new Mongoose.mongo.ObjectId('56cb91bdc3464f14678934ca'), "name": "Eyüp", "surname": "İbişoğlu", "location": { "latitude":  41.9283948, "longitude": 18.3920394 }}, callback),
		( callback ) => Driver.create({ "name": "Ahmet", "surname": "Sertaç", "location": { "latitude":  57.9283948, "longitude": 12.3920394 }}, callback),
		( callback ) => Driver.create({ "name": "Kazım", "surname": "Hakan", "location": { "latitude":  80.9283948, "longitude": 77.3920394 }}, callback),
		( callback ) => Driver.create({ "name": "Selda", "surname": "Yıldırım", "location": { "latitude":  20.9283948, "longitude": 42.3920394 }}, callback),
		( callback ) => Driver.create({ "name": "Çağatay", "surname": "İpek", "location": { "latitude":  5.9283948, "longitude": 55.3920394 }}, callback),
		( callback ) => Driver.create({ "name": "Yekta", "surname": "Candan", "location": { "latitude":  28.9283948, "longitude": 38.3920394 }}, callback)
	],
	function (error)
	{
		if ( error )
			console.log('#error', error)
		
		done()
	})
	
	
});

after(function(done)
{
	Driver.remove({}).exec()
	done()
});


describe(SERVICE_PATH, () => 
{
	it('should get drivers', (done) => 
	{
		chai.request(server).get(SERVICE_PATH).end((err, res) => 
		{
			res.should.have.status(200);
			res.body.length.should.equal(6);

			done();
		});
	});

	it('should get driver that has id (56cb91bdc3464f14678934ca)', (done) => 
	{
		chai.request(server).get(SERVICE_PATH + '/56cb91bdc3464f14678934ca').end((err, res) => 
		{
			res.should.have.status(200);
			res.body.should.be.a('object');

			done();
		});
	});

	it('should update driver that has id (56cb91bdc3464f14678934ca)', (done) => 
	{
		chai.request(server).put(SERVICE_PATH + '/56cb91bdc3464f14678934ca').send({ "driver" : { "name": "Ersin" }}).end((err, res) => 
		{
			Driver.findOne({ "_id": "56cb91bdc3464f14678934ca" }, function(error, driver)
			{
				driver.name.should.be.equal('Ersin')
				res.should.have.status(200);
			
				done();
			})

		});
	});

	it('should delete driver that has id (56cb91bdc3464f14678934ca)', (done) => 
	{
		chai.request(server).delete(SERVICE_PATH + '/56cb91bdc3464f14678934ca').send({ "driver" : { "name": "Ersin" }}).end((err, res) => 
		{
			Driver.findOne({ "_id": "56cb91bdc3464f14678934ca" }, function(error, driver)
			{
				console.log('#', driver)
				should.not.exist(driver)
				res.should.have.status(200);
			
				done();
			})

		});
	});

	it('should return closest 3 drivers.', (done) => 
	{
		chai.request(server).post(SERVICE_PATH + '/ride').send({ "latitude": 30.0003021, "longitude": 40.0201023 }).end((err, res) => 
		{
			res.body.length.should.equal(3);		
			res.body[0].name.should.equal('Yekta')
			res.body[1].name.should.equal('Selda')
			res.body[2].name.should.equal('Çağatay')
			res.should.have.status(200);
					
			done();

		});
	});
});

/*
describe('GET /api/v1/users/:tc', () => 
{
	it('it should GET the user', (done) => 
	{
		MongoClient.connect(databaseConnection).then(function (db) 
		{ 
			db.collection('users').update({ "tc": "10000000001" }, { $set: { "tc": "10000000001", active: true }}, { upsert: true }, function(err, result)
			{
		        should.not.exist(err);
		        
				chai.request(server).get('/api/v1/users/10000000001').end((err, res) => 
				{
					res.should.have.status(200);
					should.exist(res.body.data);
					res.body.data.should.be.a('object');
					res.body.ok.should.be.true;
					done();
				});
			});

		}).catch(function (error) 
		{
			console.error('Mongo > Connection > error:', error)
		})	

	})


});		
*/