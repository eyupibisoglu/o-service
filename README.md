# README #

### What is this repository for? ###

Web services for drivers. 

### What are endpoints. ###

* GET    /api/v1/drivers
> Get list of drivers.
* GET    /api/v1/drivers/:id
> Get driver that has id.
* POST   /api/v1/drivers
> Creating new driver.
* PUT    /api/v1/drivers/:id
> Update driver that has id.
* DELETE /api/v1/drivers/:id
> Delete driver that has id.
* POST   /api/v1/drivers/ride
> Get closest 3 drivers to coordinates that is sent. It takes two parameters these are longitude and latitude to calculate distance between points.      

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/d1ca694755f1477eecd4)

### How do I get set up? ###

first run to install packages:  
`npm install`  

after that:  
`npm start`

### How do I test? ###

`npm test`