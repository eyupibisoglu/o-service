var express = require('express')
var BodyParser = require('body-parser');
var cors = require('cors')

var App = express()
App.use(BodyParser.json());
App.use(BodyParser.urlencoded({ extended: true }));
App.use(cors())

App.use('/api/v1/drivers', require('./services/web/DriverService'))


App.listen(4000, function()
{
	console.log('#Up', require('moment-timezone')().tz('Europe/Istanbul').format('DD.MM.YYYY HH:mm:ss') )
})


module.exports = App; // For testing