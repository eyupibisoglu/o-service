var Mongoose = require('mongoose')
var schema = new Mongoose.Schema(
{
	"name":      { "type": String, "required": true },
	"surname":   { "type": String, "required": true },
	"location":
	{
		"latitude":  { "type": Number, "required": true },
		"longitude": { "type": Number, "required": true },
		"address":   { "type": String }
	}
		
}, { versionKey: false });
module.exports = schema;

	