var Mongoose = require('mongoose');
// ### Globals ###
var globals = require('../../globals.js')

// ### Database ###
var connection = Mongoose.createConnection( globals.database() );
var Driver = connection.model("Driver", require('../../models/Driver'));


// ### Routes ###
var router = require('express').Router()



router.get('/:id', function (req, res)
{	
	Driver.findOne({ "_id": req.params.id }).exec(function ( error, driver )
	{
		if ( !error )
			res.json(driver)
		else
			res.status(500).json(error)
	})
})


router.get('/', function (req, res)
{	
	Driver.find().lean().exec(function ( error, drivers )
	{
		if ( !error )
			res.json(drivers)
		else
			res.status(500).json(error)
	})
})


router.post('/ride', function (req, res)
{	
	var latitude  = req.body.latitude
	var	longitude = req.body.longitude



	Driver.aggregate(
	[
		{
			$project: 
			{ 
				"distance": 
				{
					$sqrt:
					{
		                $add: [
							{ $pow: [ { $subtract: [ "$location.longitude", longitude ] }, 2 ] },
							{ $pow: [ { $subtract: [ "$location.latitude",  latitude ] }, 2 ] }
		               ]
					}
	            },
	            "name": "$name",
	            "surname": "$surname" 
			}
									
		},
		{ $sort : { distance: 1 } },
		{ $limit : 3 }
	], 
	function (error, drivers) 
	{
        if ( !error )
			res.status(200).json(drivers)
		else
			res.status(500).json(error)	
    })
})

router.post('/', function (req, res)
{
	Driver.create(req.body.driver, function( error, driver )
	{
		if ( !error )
			res.status(201).json(driver)
		else
			res.status(500).json(error)	
	})

})

router.delete('/:id', function (req, res)
{
	Driver.remove({ "_id": req.params.id }, function( error, user )
	{
		if ( !error )
			res.status(200).json(user)
		else
			res.status(500).json(error)	
	})

})

router.put('/:id', function (req, res)
{
	Driver.update({ "_id": req.params.id }, { "$set": req.body.driver }, function( error, driver )
	{
		if ( !error )
			res.status(200).json(driver)
		else
			res.status(500).json(error)	
	})

})


module.exports = router