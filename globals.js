var globals = 
{
	"database": function ()
	{
		if ( process.env.NODE_ENV && process.env.NODE_ENV == 'development' )
			return "mongodb://localhost:27017/olev_test"
		else
			return "mongodb://olev:olev@ds143388.mlab.com:43388/olev"
	},
	"project":
	{
		"name": "olev",
		"port": 4000
	}
}
module.exports = globals;